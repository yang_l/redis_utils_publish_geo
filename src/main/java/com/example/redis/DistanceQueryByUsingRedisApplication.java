package com.example.redis;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@SpringBootApplication
public class DistanceQueryByUsingRedisApplication implements ApplicationContextAware {

    private static ApplicationContext ac;

    public static void main(String[] args) {
        SpringApplication.run(DistanceQueryByUsingRedisApplication.class, args);
//        String[] beans = ac.getBeanDefinitionNames();
//        for (String bean : beans) {
//            System.out.println(bean + " of Type :: " + ac.getBean(bean).getClass());
//        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ac = applicationContext;
    }


}
