package com.example.redis.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/22 11:28
 * @注释
 */
@Data
@TableName("merchant")
public class Merchant {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 地址名称
     */
    @NotBlank
    @TableField("name")
    private String name;

    /**
     * 经度
     */
    @NotNull
    @TableField("longitude")
    private Double longitude;

    /**
     * 维度
     */
    @NotNull
    @TableField("latitude")
    private Double latitude;

}
