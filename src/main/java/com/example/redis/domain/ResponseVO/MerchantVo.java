package com.example.redis.domain.ResponseVO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author YangLi
 * @Date 2023/11/24 16:14
 * @注释
 */
@Data
public class MerchantVo {

    /**
     * id
     */
    private Long id;

    /**
     * 地址名称
     */
    private String name;

    /**
     * 经度
     */
    private Double longitude;

    /**
     * 维度
     */
    private Double latitude;

    /**
     * 距离
     */
    private Double distance;
}
