package com.example.redis.bo;

import com.example.redis.constant.RedisConstant;
import lombok.*;


/**
 * <p>
 * Redis GEO 坐标信息
 * </p>
 *
 * @author yang_li
 */
@Data
public class RedisGeoPoint {

    /**
     * key  这是geo集合的 名称
     */
    private String key = RedisConstant.REDIS_GEO_NAME;
    /**
     * members
     */
    private String member;
    /**
     * 经度
     */
    private Double lng;
    /**
     * 纬度
     */
    private Double lat;

    public static RedisGeoPointBuilder builder() {
        return new RedisGeoPointBuilder();
    }

    public RedisGeoPoint(String key, String member, Double lng, Double lat) {
        this.key = key;
        this.member = member;
        this.lng = lng;
        this.lat = lat;
    }

    public RedisGeoPoint() {
    }

    public static class RedisGeoPointBuilder {
        private String key;
        private String member;
        private Double lng;
        private Double lat;

        RedisGeoPointBuilder() {
        }



        public RedisGeoPointBuilder key(String key) {
            this.key = key;
            return this;
        }

        public RedisGeoPointBuilder member(String member) {
            this.member = member;
            return this;
        }

        public RedisGeoPointBuilder lng(Double lng) {
            this.lng = lng;
            return this;
        }

        public RedisGeoPointBuilder lat(Double lat) {
            this.lat = lat;
            return this;
        }

        public RedisGeoPoint build() {
            return new RedisGeoPoint(key, member, lng, lat);
        }

        @Override
        public String toString() {
            return "RedisGeoPoint.RedisGeoPointBuilder(key=" + this.key + ", member=" + this.member + ", lng=" + this.lng + ", lat=" + this.lat + ")";
        }
    }
}
