package com.example.redis.config;

import com.example.redis.constant.RedisConstant;
import com.example.redis.listener.DemoRedisTestListener;
import com.example.redis.listener.RedisTestListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * <p>
 * redis监听事件配置
 * </p>
 *
 * @author yang_li
 * 这里加入发布订阅配置
 */
@Configuration
public class RedisListenerConfig {

    @Bean
    RedisMessageListenerContainer redisContainer(RedisConnectionFactory factory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        // 这里添加两个监听者   分别监听两个频道
        container.addMessageListener(
                new MessageListenerAdapter(new DemoRedisTestListener()),
                // 监听的频道是  RedisConstant.REDIS_CHANNEL_DEMO
                new ChannelTopic(RedisConstant.REDIS_CHANNEL_DEMO)
        );
        container.addMessageListener(
                new MessageListenerAdapter(new RedisTestListener()),
                // 监听的频道是  RedisConstant.REDIS_CHANNEL_TEST
                new ChannelTopic(RedisConstant.REDIS_CHANNEL_TEST)
        );
        return container;
    }
    
}