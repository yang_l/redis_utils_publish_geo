package com.example.redis.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> Redisson配置类 </p>
 *
 * @author yang_li
 */
@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private String port;

//    @Value("${spring.redis.password}")
//    private String password;

    @Value("${spring.redis.database}")
    private Integer database;

    @Value("${spring.redis.jedis.pool.min-idle}")
    private Integer connectionMinimumIdleSize;

    @Value("${spring.redis.timeout}")
    private Integer timeout;

    @Bean("redisson")
    public RedissonClient getRedisson() {
        Config config = new Config();
        config
                .useSingleServer()
                .setAddress("redis://" + this.host + ":" + this.port)
                // 如果设置了密码则需要发送密码
                //.setPassword(this.password)
                .setDatabase(this.database)
                .setConnectionMinimumIdleSize(this.connectionMinimumIdleSize)
                .setTimeout(this.timeout);
        return Redisson.create(config);
    }

}
