package com.example.redis.util;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Redisson工具类
 * </p>
 *
 * @author yang_li
 */
@Component
public class RedissonUtil {

    private static RedissonClient redissonClient;

    @Autowired
    public RedissonUtil(RedissonClient redissonClient) {
        RedissonUtil.redissonClient = redissonClient;
    }


    // ============================ ↓↓↓↓↓↓ 操作 锁 ↓↓↓↓↓↓ ============================

    /**
     * 可重入锁
     *
     * @param key key
     * @return 锁
     */
    public static RLock getLock(String key) {
        return redissonClient.getLock(key);
    }

    /**
     * 可重入锁
     *
     * @param key       key
     * @param leaseTime 时间
     * @param unit      时间单位
     * @return 锁
     * @author yang_li
     * @date 2022/1/14 9:25 下午
     */
    public static RLock lock(String key, long leaseTime, TimeUnit unit) {
        RLock lock = redissonClient.getLock(key);
        // 加锁leaseTime以后自动解锁
        // 无需调用unlock方法手动解锁
        lock.lock(leaseTime, unit);
        return lock;
    }

}
