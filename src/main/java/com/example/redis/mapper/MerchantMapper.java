package com.example.redis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.redis.bo.RedisGeoPoint;
import com.example.redis.domain.entity.Merchant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/22 16:11
 * @注释
 */
@Mapper
public interface MerchantMapper extends BaseMapper<Merchant> {

    /**
     * 通过经纬度来查询数据库中的商户数据  但是由于存入redis中经纬度 查询出来会有精度问题  所以这个方法也就不适用了
     *
     * @param redisGeoPoints ·
     * @return ·
     */
    List<Merchant> selectMerchantByLngAndLat(List<RedisGeoPoint> redisGeoPoints);

    int insertBackId(Merchant merchant);
}
