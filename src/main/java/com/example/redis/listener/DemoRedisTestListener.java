package com.example.redis.listener;

import com.example.redis.utils.RedisTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;


/**
 * <p> 发布订阅 -- 测试消费者 </p>
 *
 * @author yang_li
 */
@Slf4j
@Component
@Order(1000)
public class DemoRedisTestListener implements MessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String msg = new String(message.getBody());
        log.info("频道[demo] 收到订阅消息：{}", msg);
        RedisTemplateUtil.setOpsForValue("12", "test");
    }

}