package com.example.redis.globalexception;

import com.alibaba.fastjson.JSON;
import lombok.Data;

/**
 * @author yang_li
 */
@Data
public class ResultResponse {
    /**
     * 响应代码
     */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object result;

    public ResultResponse() {
    }


    public ResultResponse(BaseErrorInfoInterface errorInfo) {
        this.code = errorInfo.getCode();
        this.message = errorInfo.getMsg();
    }


    /**
     * 成功
     *
     * @return 1
     */
    public static ResultResponse success() {
        return success(null);
    }

    /**
     * 成功
     *
     * @param data 1
     * @return 1
     */
    public static ResultResponse success(Object data) {
        ResultResponse resultResponse = new ResultResponse();
        resultResponse.setCode(ExceptionEnum.SUCCESS.getCode());
        resultResponse.setMessage(ExceptionEnum.SUCCESS.getMsg());
        resultResponse.setResult(data);
        return resultResponse;
    }

    /**
     * 失败
     */
    public static ResultResponse error(BaseErrorInfoInterface errorInfo) {
        ResultResponse resultResponse = new ResultResponse();
        resultResponse.setCode(errorInfo.getCode());
        resultResponse.setMessage(errorInfo.getMsg());
        resultResponse.setResult(null);
        return resultResponse;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String code, String message) {
        ResultResponse resultResponse = new ResultResponse();
        resultResponse.setCode(code);
        resultResponse.setMessage(message);
        resultResponse.setResult(null);
        return resultResponse;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String message) {
        ResultResponse resultResponse = new ResultResponse();
        resultResponse.setCode("-1");
        resultResponse.setMessage(message);
        resultResponse.setResult(null);
        return resultResponse;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}