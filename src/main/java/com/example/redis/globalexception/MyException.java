package com.example.redis.globalexception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yang_li
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MyException extends RuntimeException { // 自定义异常异常  自己new出来 手动抛出去  比如 该号码已经注册过...

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    protected final String errorCode;
    /**
     * 错误信息
     */
    protected final String errorMsg;

    public MyException(String errorCode, String errorMsg) {
        super();
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public MyException(BaseErrorInfoInterface errorInfoInterface) {
        super(errorInfoInterface.getCode());
        this.errorCode = errorInfoInterface.getCode();
        this.errorMsg = errorInfoInterface.getMsg();
    }

    public MyException(BaseErrorInfoInterface errorInfoInterface, Throwable cause) {
        super(errorInfoInterface.getCode(), cause);
        this.errorCode = errorInfoInterface.getCode();
        this.errorMsg = errorInfoInterface.getMsg();
    }

    public MyException(String errorCode, String errorMsg, Throwable cause) {
        super(errorCode, cause);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
