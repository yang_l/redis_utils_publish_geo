package com.example.redis.controller;

import com.example.redis.bo.RedisGeoPoint;
import com.example.redis.constant.RedisConstant;
import com.example.redis.domain.ResponseVO.MerchantVo;
import com.example.redis.globalexception.MyException;
import com.example.redis.globalexception.ResultResponse;
import com.example.redis.mapper.MerchantMapper;
import com.example.redis.util.RedisGeoUtil;
import org.springframework.data.geo.Distance;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import com.example.redis.domain.entity.Merchant;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @Author YangLi
 * @注释
 */
@RequestMapping("/redis")
@RestController
public class RedisTestController {

    private static final Integer ELEVEN = 11;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private MerchantMapper merchantMapper;

    @GetMapping("publish")
    public void publishTest() {
        // 可以发送请求测试   也可以直接在redis控制台  向频道发布消息  命令为：  publish your_channel  message
        // 也可以在终端订阅消息  命令为： subscribe your_channel
        redisTemplate.convertAndSend(RedisConstant.REDIS_CHANNEL_DEMO, "这是一条发给demo频道的消息");
        redisTemplate.convertAndSend(RedisConstant.REDIS_CHANNEL_TEST, "这是一条发给test频道的消息");
    }

    /**
     * 想要测试使用redis查询距离  需要先放一些数据在redis中  不然无法测试 这里先添加几个商户
     * 在添加商户的同时将商户的地址存入redis中，然后在测试距离的实现
     */
    @PostMapping("addMerchant")
    @Transactional(rollbackFor = Exception.class)
    public ResultResponse addMerchant(@RequestBody @Valid Merchant merchant) {
        // 这里就不一层一层走了  直接将mapper注入到controller中  直接调用
        // 这里主键回填 需要注意的是 回填是回填在这个参数类中  而不是直接返回回来
//        List<Merchant> merchants = merchantMapper.selectList(new QueryWrapper<>());
//        merchants.forEach(merchant1 -> RedisGeoUtil.geoAdd(new RedisGeoPoint(RedisConstant.REDIS_GEO_NAME,merchant1.getId().toString(),merchant1.getLongitude(), merchant1.getLatitude())));
        merchantMapper.insertBackId(merchant);
        Long id = merchant.getId();
        if (id == null)
            return ResultResponse.error("添加用户失败");
        // 这里的返回值 成功是1  失败是0
        Long aLong = RedisGeoUtil.geoAdd(new RedisGeoPoint(RedisConstant.REDIS_GEO_NAME, id.toString(), merchant.getLongitude(), merchant.getLatitude()));
        if (aLong == 0)
            //添加地址失败的情况商户也应该是添加失败   不然会出现找不到商户地址的情况  所以在上面加一个@Transactional(rollbackFor = Exception.class)
            throw new MyException("6379", "地址添加失败");
        return ResultResponse.success();
    }

    /**
     * 重点注意   一来是想的是将经纬度存入redis  RedisGeoPoint  这个类中   查询的时候通过  经纬度和数据库中的经纬度来匹配商户
     * 但是实际上这种方式不可行   redis中会对经纬度进行小数位的扩展   比如  我们将  11.11，22.22  这个经纬度存入数据库的同时  也存入redis
     * 在数据库中的数据就是 11.11，22.22   但是  redis查询出来的  11.11xxxxxxxxx , 22.22xxxxxxxx   redis中的算法会扩展经度  所以
     * 不能通过 redis中的经纬度来直接查询数据库中的商户   所以我们在往redis和数据存值的时候给一个唯一标识  直接在member中存入唯一字段   这里选择存入数据库中的id
     */


    /**
     * 查询当前位置多少距离内的所有商户,并且返回到各个商户的距离
     *
     * @param longitude 经度
     * @param latitude  维度
     * @param distance  距离
     * @return ·
     */
    @GetMapping("queryMerchantWithinDistance")
    public ResultResponse queryMerchantWithinDistance(@RequestParam Double longitude,
                                                      @RequestParam Double latitude,
                                                      @RequestParam Double distance) {
        // 找到redis 中满足距离要求的数据
        List<RedisGeoPoint> redisGeoPoints = RedisGeoUtil.geoNear(RedisConstant.REDIS_GEO_NAME, longitude, latitude, distance, RedisGeoCommands.DistanceUnit.METERS, ELEVEN);
        ArrayList<MerchantVo> list = new ArrayList<>();
        // 求出点前位置到满足条件的商户的距离并放进merchantVO中 在收集成一个集合
        redisGeoPoints.forEach(item -> {
            MerchantVo merchantVo = new MerchantVo();
            if (Long.parseLong(item.getMember()) != -1)
                merchantVo.setId(Long.valueOf(item.getMember()));
            Distance distance1 = RedisGeoUtil.geoDistByLngAngLat(RedisConstant.REDIS_GEO_NAME, longitude, latitude, item.getMember(), RedisGeoCommands.DistanceUnit.METERS);
            merchantVo.setDistance(distance1.getValue());
            list.add(merchantVo);
        });
        int index = -1;
        for (MerchantVo merchantVo : list) {
            if (merchantVo.getId() == null)
                index = list.indexOf(merchantVo);
        }
        if (index != -1) {
            list.remove(index);
        }

        if (ObjectUtils.isEmpty(list))
            return ResultResponse.success();
        // 根据上面的集合拿到id 然后去数据库查询商户数据 在根据id 将merchants中的数据放进MerchantVo中 再收集成一个集合最终返回
        List<Merchant> merchants = merchantMapper.selectBatchIds(list.stream().filter(item -> item.getId() != null).map(MerchantVo::getId).collect(Collectors.toList()));
        list.forEach(item -> merchants.forEach(merchant -> {
            if (merchant.getId().equals(item.getId())) {
                item.setLatitude(merchant.getLatitude());
                item.setLongitude(merchant.getLongitude());
                item.setName(merchant.getName());
            }
        }));
        return ResultResponse.success(list);
    }

    @GetMapping("test")
    public Object test(){
        redisTemplate.opsForValue().set("adbf", 10);
//        Object bb = redisTemplate.opsForValue().get("adbf");
        Object aa = redisTemplate.opsForValue().get("rfkeyReplayCount");
        if (aa != null){
            System.out.println("sss");
        }else System.out.println("null");
        return aa;
    }


}
