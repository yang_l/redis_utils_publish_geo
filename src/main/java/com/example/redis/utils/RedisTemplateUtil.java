package com.example.redis.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author YangLi
 * @Date 2024/4/8 9:52
 * @注释
 */
@Component
public class RedisTemplateUtil {


    public static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public RedisTemplateUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisTemplateUtil.redisTemplate = redisTemplate;
    }

    public static Object getOpsForValue(String key){
        return  redisTemplate.opsForValue().get(key);
    }

    public static void setOpsForValue(String key, Object value){
        redisTemplate.opsForList().rightPush(key, value);
    }

}
