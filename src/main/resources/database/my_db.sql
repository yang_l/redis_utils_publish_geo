/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : my_db

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 24/11/2023 17:54:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of merchant
-- ----------------------------
INSERT INTO `merchant` VALUES (1, '成都西站', 103.981985, 30.689223);
INSERT INTO `merchant` VALUES (2, '成都第一骨科医院', 103.995329, 30.686579);
INSERT INTO `merchant` VALUES (3, '王府井百货', 104.08648, 30.6641);
INSERT INTO `merchant` VALUES (4, '成都太古里', 104.090145, 30.6641);
INSERT INTO `merchant` VALUES (5, '成都成华区万象城', 104.121909, 30.65426);
INSERT INTO `merchant` VALUES (6, '成都339购物中心', 104.100566, 30.6674);
INSERT INTO `merchant` VALUES (7, '成都花果新区一居', 104.123607, 30.630021);
INSERT INTO `merchant` VALUES (8, '成都望江宾馆', 104.123607, 30.632911);
INSERT INTO `merchant` VALUES (9, '成都镏金岁月', 104.113169, 30.635055);
INSERT INTO `merchant` VALUES (10, '成都望江校区', 104.089992, 30.637012);
INSERT INTO `merchant` VALUES (11, '成都东站', 104.148634, 30.637261);
INSERT INTO `merchant` VALUES (12, '省体育馆', 104.072817, 30.639747);
INSERT INTO `merchant` VALUES (13, '金沙博物馆遗址', 104.019134, 30.687616);

SET FOREIGN_KEY_CHECKS = 1;
